/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxhelloworld;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Mensajes {
  private String saludo;
  private String despedida;
  
  public Mensajes(){
    saludo = "Hello World";
    despedida = "kthxbai";
  }
  
  public String getSaludo(){
    return saludo;
  }
  
  public String getDespedida(){
    return despedida;
  }
  
}
