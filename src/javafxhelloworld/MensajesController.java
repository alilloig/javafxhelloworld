/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxhelloworld;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;

/**
 * FXML Controller class
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class MensajesController implements Initializable {
  @FXML private Label etiqueta;
  private Mensajes mensajes;

  @FXML private void mostrarSaludo(ActionEvent action) {
    etiqueta.setText(mensajes.getSaludo());
  }
  
  @FXML private void mostrarDespedida(ActionEvent action){
    etiqueta.setText(mensajes.getDespedida());
  }
  
  @Override
  public void initialize(URL url, ResourceBundle rb) {
    mensajes = new Mensajes();
  }  
  
}
